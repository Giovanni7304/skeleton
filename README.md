## Skeleton

This repository provides a sample Go Skeleton app that you may use when bootstrapping a new Go project with GitLab CI.

Initial setup of the directory

>git clone git@gitlab.com:Skeleton.git
>cd Skeleton
skeleton>go mod init gitlab.com/Skeleton

skeleton>git add go.mod 
skeleton>git commit -m "Initial Commit"
skeleton>git branch
* master 
skeleton>git push origin master

Then go to gitlab.com 

skeleton>git checkout -b doc 
skeleton>git branch
*doc 
master
skeleton>git add doc.go 
skeleton>git commit -m "add doc.go file"
skeleton>git push origin doc

Then to to gitlab and merge


skeleton>git checkout master
skeleton>git pull
skeleton>git config pull.ff only
skeleton>git branch --merged
skeleton>git branch -d doc
skeleton>go test 
